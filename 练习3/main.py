'''
练习3 180801206 季傲伟
'''
n = int(input("输入打印的行数："))
if n > 10 or n < 1:
    print("输入有误，请输入1-10之内的数！")
else:
    for i in range(n):
        print(' ' * (n - i) + '=' * (2 * i + 1))
