'''
练习5 180801206 季傲伟
'''
import turtle
a=int(input("等边三角形边长"))
b=int(input("圆的半径"))
print(a in range(100,200))
print(b in range(100,200))
t=turtle.Turtle(shape='turtle')
i = 0
while i < 3:
    t.forward(a)
    t.left(120)
    i=i+1

t.penup()
t.goto(200,200)
t.pendown()
t.circle(b)
turtle.mainloop()

