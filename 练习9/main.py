'''
练习9 180801206 季傲伟
'''

def read_file():
    with open('G:\TheOldManAndtheSea.txt', 'r', encoding='utf-8') as f:
        word = []
        for word_str in f.readlines():
            word_str = word_str.replace(',', '')
            word_str = word_str.strip()
            word_list = word_str.split(' ')
            word.extend(word_list)
        return word


def clear_account(lists):
    count_dict = {}
    count_dict = count_dict.fromkeys(lists)
    word_list1 = list(count_dict.keys())
    for i in word_list1:
        count_dict[i] = lists.count(i)
    return count_dict


def sort_dict(count_dict):
    del [count_dict['']]
    my_dict = sorted(count_dict.items(), key=lambda d: d[1], reverse=True)
    my_dict = dict(my_dict)

    return my_dict

def main(my_dict):
    i = 0
    for x, y in my_dict.items():
        if i < 10:
            print('单词"%s",出现次数为 %s' % (x, y))
            i += 1
            continue
        else:
            break

main(sort_dict(clear_account(read_file())))